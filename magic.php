<?php

class person
{
    public $name="Test Name";
    public $phone="0184569742";
    public $date_of_birth="13/05/1991";
    public static $message = "static property value";

    public static function dofrequently(){
        echo "im doing it frequently.<br>";
    }

    public static function doSomething(){
        echo "im doing it frequently.<br>";
    }

    public function __construct()
    {
        echo "im inside the" . __METHOD__ . "<br>";
    }

    public function __destruct()
    {
        echo "im dying inside the" . __METHOD__ . "<br>";
    }

    public function __call($name, $arguments)
    {
        echo "i am inside the" . __METHOD__ . "<br>";
        echo "name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";

    }

    public function __callstatic($name, $arguments)
    {
        echo "i am inside the" . __METHOD__ . "<br>";
        echo "wrong static name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";

    }



    public function __set($name, $value){
        echo __METHOD__ ."Wrong property name=$name<br>";
       // echo "Wrong property name= $name.<br> ";
        echo "Wrong value=$value.<br>";

    }

    public function __get($name)
    {
        echo __METHOD__ . "Wrong Property name=$name<br>";
        echo "Wrong property to be read:.$name";
    }


    public function __isset($name)
    {
        echo __METHOD__."Wrong Property Name = $name.<br>";
    }

    public function __unset($name)
    {
        echo __METHOD__."Wrong Property Name = $name.<br>";
    }

    public function __sleep()
    {
        return array("name","phone");
    }

    public function __wakeup()
    {
       $this->doSomething();
    }

   public function __toString()
   {
       return "Are you crazy?!!!! I am an object!!!"."<br>";
   }
    public function __invoke()
    {
        echo "I am an object but you are trying to call me as a function!!!!"."<br>";
    }
}
Person::$message="static";//static property setting

echo Person::$message."<br>";//static property reading test



Person::dofrequently("Wrong static para 1");  //static method call

$obj = new person(); //object creating
$obj->dosomething(); //calling a function
$obj->habijabi("para"); // calling a wrong function

$obj->address="Agrabad,Chittagong"; //calling a wrong property
echo "$obj->address.<br>";

if(isset($obj->nai)){

}
echo "<br>";
unset($obj->nai);

echo "<br>";
$myvar=serialize($obj);
echo "<pre>";
var_dump($myvar);
echo "</pre>";

echo "<br>";

$myvar1=unserialize($myvar);
echo "<pre>";
var_dump($myvar1);
echo "</pre>";

echo $obj;  //toString()

$obj();


?>